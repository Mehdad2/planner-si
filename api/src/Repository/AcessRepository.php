<?php

namespace App\Repository;

use App\Entity\Acess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Acess|null find($id, $lockMode = null, $lockVersion = null)
 * @method Acess|null findOneBy(array $criteria, array $orderBy = null)
 * @method Acess[]    findAll()
 * @method Acess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Acess::class);
    }

    // /**
    //  * @return Acess[] Returns an array of Acess objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Acess
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
