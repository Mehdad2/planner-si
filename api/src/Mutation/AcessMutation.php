<?php
namespace App\Mutation;

use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use App\Repository\UsersRepository;
use App\Repository\AcessRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class AcessMutation implements MutationInterface, AliasedInterface
{
    private $em;
    private $jwtmanager;
    /**
     * @var UsersRepository
     */
    private $userRepository;

    /**
     * @var AcessRepository
     */
    private $acessRepository;
    private $jwtencode;

    public function __construct(
        EntityManagerInterface $em, 
        UsersRepository $userRepository,
        AcessRepository $acessRepository,
        JWTTokenManagerInterface $jwtmanager,
        JWTEncoderInterface $jwtencode
        )
    {
        $this->em = $em;
        $this->jwtmanager = $jwtmanager;
        $this->userRepository = $userRepository;
        $this->acessRepository = $acessRepository;
        $this->jwtencode = $jwtencode;
    }

    public function resolve(
        int $userId,
        string $password
        )
    {
        $userData = $this->userRepository->findOneBy(['id' => $userId]);
        $acessData = $this->acessRepository->findOneBy(['user' => $userData->getId()]);
        $hash = $acessData->getPassword();
        $verify = password_verify($password, $hash);
        if($verify) {
            if(!$acessData->getRefreshToken()) {
                try {
                    $this->jwtencode->decode($acessData->getToken());
                } catch(\Exception $e) { 
                    $refreshToken = $this->jwtmanager->create($userData);
                    $acessData->setRefreshToken($refreshToken);
                    $this->em->persist($acessData);
                    $this->em->flush();
                }
            } else {
                try {
                    $this->jwtencode->decode($acessData->getRefreshToken());
                } catch(\Exception $e) { 
                    $refreshToken2 = $this->jwtmanager->create($userData);
                    $acessData->setRefreshToken($refreshToken2);
                    $this->em->persist($acessData);
                    $this->em->flush();
                }
            }
    
            return ['content' => 'new acess created'];
        }
        return ['content' => 'invalid information user'];
        
        
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'NewAcess',
        ];
    }
}