<?php
namespace App\Mutation;

use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use App\Entity\Acess;
use App\Entity\Users;
use App\Entity\Tokens;
use App\Repository\UsersRepository;
use App\Repository\AcessRepository;
use App\Repository\TokensRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class LoginMutation implements MutationInterface, AliasedInterface
{
    private $em;
      /**
     * @var UsersRepository
     */
    private $userRepository;
     /**
     * @var AcessRepository
     */
    private $acessRepository;
     /**
     * @var TokensRepository
     */
    private $tokenRepository;
    protected $encoder;
    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtmanager;

    private $jwtencode;


    public function __construct(
        EntityManagerInterface $em, 
        UsersRepository $userRepository,
        AcessRepository $acessRepository,
        TokensRepository $tokenRepository,
        UserPasswordEncoderInterface $encoder,
        JWTTokenManagerInterface $jwtmanager,
        JWTEncoderInterface $jwtencode
         )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->acessRepository = $acessRepository;
        $this->tokenRepository = $tokenRepository;
        $this->encoder = $encoder;
        $this->jwtmanager = $jwtmanager;
        $this->jwtencode = $jwtencode;

    }

    public function resolve(
        string $pseudo,
        string $password
        )
    {
        $users = new Users();
        $acess = new Acess();
        $tokens = new Tokens();
        $userData = $this->userRepository->findOneBy(['pseudo' => $pseudo]);

        /**
         * we check pseudo
         */
       if($userData !== null) {
        $acessData = $this->acessRepository->findOneBy(['user' => $userData->getId()]);
        $hash = $acessData->getPassword();
        $verify = password_verify($password, $hash);
        $date = new \DateTime();
        /**
         * we check password
         */
        if($verify) {
            $token = $this->jwtmanager->create($userData);
            $existToken = $this->acessRepository->findOneBy(['token' => $token]);
            $existRefreshToken = $this->acessRepository->findOneBy(['refreshToken' => $token]);
            $existOldRefreshToken = $this->tokenRepository->findOneBy(['oldToken' => $token]);
            /*if($acessData->getToken() !== null) {
                //$tokenExp = $this->jwtencode->decode($userData->getTokens()->getOldToken());
                $tokenData = $this->tokenRepository->findOneBy(['users' => $userData->getId()]);
                $oldtoken = $acessData->getToken();*/
                /**
                 * we set olderToken based on token or refreshtoken 
                 */
               /* if($tokenData !== null){
                    if($acessData->getRefreshToken() !== null) {
                        $oldrefreshtoken = $acessData->getRefreshToken();
                        $tokenData->setOldToken($oldrefreshtoken);
                    } else {
                        $tokenData->setOldToken($oldtoken);
                        $this->em->persist($tokenData);
                        $this->em->flush();
                    }
                } else {
                    $userData->setTokens($tokens->setOldToken($oldtoken));
                    $this->em->persist($userData);
                    $this->em->flush();
                }
            }*/

            /**
             * if token or refresh exist we check that the new one created is different for each connexion
             */
            if($existOldRefreshToken !== null)
            //|| $existToken !== null ||$existRefreshToken !== null 
             {
                return ['content' => 'invalid token'];
            } else {
                $acessData->setToken($token);
                $this->em->persist($acessData);
                $this->em->flush();
                return ['content' => 'valid login','code' => 200, 'id' => $userData->getId(), 'pseudo' => $userData->getPseudo(), 'code' => 200 ];
            }
           
        }
        return ['content' => 'invalid pass', 'code' => 404];
       
       }
       return ['content' => 'invalid pseudo', 'code' => 404];
    }

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Login',
        ];
    }
}