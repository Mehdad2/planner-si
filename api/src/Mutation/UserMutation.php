<?php
namespace App\Mutation;

use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use App\Entity\Users;
use App\Entity\Acess;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;




class UserMutation implements MutationInterface, AliasedInterface
{
    private $em;
    protected $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function resolve(
        string $pseudo, 
        string $email,
        string $firstName, 
        string $lastName, 
        string $roles, 
        string $gender,
        bool $taskEditored,
        bool $taskCreated,
        array $acess
        )
    {
        $users = new Users();
        $acessI = new Acess();

        $users->setPseudo($pseudo);
        $users->setEmail($email);
        $users->setFirstName($firstName);
        $users->setLastName($lastName);
        $users->setRoles($roles);
        $users->setGender($gender);
        $users->setTaskEditored($taskEditored);
        $users->setTaskCreated($taskCreated);
        $encoded = $this->encoder->encodePassword($users,$acess['password']);
        $users->setAcess($acessI->setPassword($encoded));
     
        $this->em->persist($users);
        $this->em->flush();

        return ['content' => 'new user created'];
    }
 

    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'NewUser',
        ];
    }
}