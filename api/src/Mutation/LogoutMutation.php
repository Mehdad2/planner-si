<?php
namespace App\Mutation;

use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use App\Repository\AcessRepository;
use App\Repository\TokensRepository;
use App\Repository\UsersRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

class LogoutMutation implements MutationInterface, AliasedInterface
{
    private $em;
        /**
     * @var UsersRepository
     */
    private $userRepository;
      /**
     * @var AcessRepository
     */
    private $acessRepository;
     /**
     * @var TokensRepository
     */
    private $tokenRepository;
    
    public  function __construct(
        EntityManagerInterface $em, 
        UsersRepository $userRepository,
        AcessRepository $acessRepository, 
        TokensRepository $tokenRepository ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->acessRepository = $acessRepository;
        $this->tokenRepository = $tokenRepository;
    }

    public function resolve(
        string $pseudo
    ) {

        $userData = $this->userRepository->findOneBy(['pseudo' => $pseudo]);
        $acessData = $this->acessRepository->findOneBy(['user' => $userData->getId()]);
        $tokenData = $this->tokenRepository->findOneBy(['users' => $userData->getId()]);
        $oldtoken = $acessData->getToken();

         if($acessData->getRefreshToken() !== '') {
            //$tokenExp = $this->jwtencode->decode($userData->getTokens()->getOldToken());
            /**
             * we set olderToken based on token or refreshtoken 
             */
            $oldrefreshtoken = $acessData->getRefreshToken();
            $tokenData->setOldToken($oldrefreshtoken);
         } else {
                $userData->setTokens($tokenData->setOldToken($oldtoken));
                $this->em->persist($userData);
                $this->em->flush();
        }
        $acessData->setToken('');
        $acessData->setRefreshToken('');
        $this->em->persist($acessData);
        $this->em->flush();
     

        return ['content' => 'sucess deconnection no rft'];

    }
  

    public static function getAliases(): array 
    {
        return [
            'resolve' => 'Logout',
        ];
    }
  
}