<?php
namespace App\Resolver;

use App\Repository\TasksRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;


class TaskResolver implements ResolverInterface, AliasedInterface
{
    /**
     * @var TasksRepository
     */
    private $taskRepository;

    /**
     *
     * @param TasksRepository $taskRepository
     */
    public function __construct(TasksRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

  
    public function resolve(int $id)
    {
        return $this->taskRepository->find($id);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Task',
        ];
    }
}