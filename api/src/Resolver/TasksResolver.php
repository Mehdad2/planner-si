<?php
namespace App\Resolver;

use App\Repository\TasksRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;


class TasksResolver implements ResolverInterface, AliasedInterface
{
    /**
     * @var TasksRepository
     */
    private $taskRepository;

    /**
     *
     * @param TasksRepository $taskRepository
     */
    public function __construct(TasksRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

  
    public function resolve()
    {
        return $this->taskRepository->findAll();
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Tasks',
        ];
    }
}