<?php
namespace App\Resolver;

use App\Repository\UsersRepository;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;


class UsersResolver implements ResolverInterface, AliasedInterface
{
    /**
     * @var UsersRepository
     */
    private $userRepository;

    /**
     *
     * @param UsersRepository $userRepository
     */
    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

  
    public function resolve()
    {
        return $this->userRepository->findAll();
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getAliases(): array
    {
        return [
            'resolve' => 'Users',
        ];
    }
}

