<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 *
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $pseudo;

    /**
     *
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Unique
     */
    private $email;

    /**
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $LastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gender;

    /**
     * @ORM\Column(type="boolean")
     */
    private $TaskEditored;

    /**
     * @ORM\Column(type="boolean")
     */
    private $TaskCreated;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tasks", inversedBy="users")
     */
    private $task;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Acess", mappedBy="user", cascade={"persist"})
     * 
     * 
     **/
     
    private $acess;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Tokens", mappedBy="users", cascade={"persist"})
     */
    private $tokens;




    public function __construct()
    {
        $this->updateDatetime();
        $this->task = new ArrayCollection();
    }

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->LastName;
    }

    public function setLastName(?string $LastName): self
    {
        $this->LastName = $LastName;

        return $this;
    }

    public function getRoles(): ?string
    {
        return $this->roles;
    }

    public function setRoles(string $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getTaskEditored(): ?bool
    {
        return $this->TaskEditored;
    }

    public function setTaskEditored(bool $TaskEditored): self
    {
        $this->TaskEditored = $TaskEditored;

        return $this;
    }

    public function getTaskCreated(): ?bool
    {
        return $this->TaskCreated;
    }

    public function setTaskCreated(bool $TaskCreated): self
    {
        $this->TaskCreated = $TaskCreated;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|Tasks[]
     */
    public function getTask(): Collection
    {
        return $this->task;
    }

    public function addTask(Tasks $task): self
    {
        if (!$this->task->contains($task)) {
            $this->task[] = $task;
        }

        return $this;
    }

    public function removeTask(Tasks $task): self
    {
        if ($this->task->contains($task)) {
            $this->task->removeElement($task);
        }

        return $this;
    }

       /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateDatetime() : void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getAcess(): ?Acess
    {
        return $this->acess;
    }

    public function setAcess(Acess $acess): self
    {
        $this->acess = $acess;

        // set the owning side of the relation if necessary
        if ($acess->getUser() !== $this) {
            $acess->setUser($this);
        }

        return $this;
    }
        /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

     /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

  
      /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        //return (string) $this->password;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getTokens(): ?Tokens
    {
        return $this->tokens;
    }

    public function setTokens(Tokens $tokens): self
    {
        $this->tokens = $tokens;

        // set the owning side of the relation if necessary
        if ($tokens->getUsers() !== $this) {
            $tokens->setUsers($this);
        }

        return $this;
    }


  

  
}
