import {createStore, applyMiddleware, compose} from 'redux';
import reducer from '../reducers';
import { persistStore, persistReducer } from 'redux-persist'
import ReduxThunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage' 
//import {  routerMiddleware  } from 'connected-react-router'
//import history from './history'

//const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const composeEnhancersProd = compose;
const middleware = composeEnhancersProd(applyMiddleware(
    ReduxThunk,
));

//const saveSubsetFilter = createFilter(reducer,'login', ['token']);
const persistConfig = {
  key: 'app-react',
  storage,
  // whitelist: ['login'],
  // transforms: [saveSubsetFilter]
}


const persistedReducer = persistReducer(persistConfig, reducer)


const store = createStore(
  persistedReducer,
  middleware
);


const persistor = persistStore(store)


export {store, persistor, persistConfig};
