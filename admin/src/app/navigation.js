import React from "react"
import { connect } from "react-redux"
import { Switch, Route, Link } from 'react-router-dom';
import MainScreen from "../containers/authentification"
import SecondScreen from "../containers/dashboard"

const Navigator = ({ isAuth }) => {
    return (
        <Switch>
           {isAuth
            ?
            <Route path='/' component={SecondScreen} />
            :
            <Route exact path='/' component={MainScreen} />
           }
        </Switch>
    )



}
const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthentificate


})

export default connect(mapStateToProps)(Navigator)