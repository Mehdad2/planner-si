import axios from "axios"
import { auth } from './types'
import { userInfo } from 'os'

export const checkAuth = (id, name) => ({
    type: auth.CHECK_LOGIN,
    id,
    name
})

const setError = (code, message) => ({
    type: auth.SET_ERRORS,
    code,
    message
})

const setStatusAuth = () => ({
    type: auth.SET_STATUSAUTH
})

export const ThunkLogin =  (user) => {
    return (dispatch, getState) => {
        const config = {
            query: `mutation Login($log: LoginInput!) {
                Login(input: $log) {
                  content,
                  code,
                  id,
                  pseudo
                }
              }`,
            variables: {
                "log": {
                    pseudo: user.pseudo,
                    password: user.pass
                }
            }
          };
        
         axios.post(`http://127.0.0.1:8000`, config)
            .then((response) => {
                if(response.data.data.Login.code !== 200) {
                    dispatch(setError(response.data.data.Login.code, response.data.data.Login.content))
                } else {
                    dispatch(setError(response.data.data.Login.code, response.data.data.Login.content))
                    dispatch(setStatusAuth())

                }
            })
            .catch((e) => {
                console.log('error ThunkLogin ' + e.message)
            })

    }

}
