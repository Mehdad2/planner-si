import React from "react"
import { connect } from "react-redux"
import LoginForm from "../../components/authentification/index"

const MainSceen = ({dispatch, log, resp}) => (
    <LoginForm {...{dispatch, log, resp}}/>

)

const mapStateToProps = (state) => ({
   log: state.auth.log,
   resp: state.auth.response
})

export default connect(mapStateToProps)(MainSceen)