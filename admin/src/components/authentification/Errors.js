import React from "react"

const Errors = (props) => {
  return (
    props[404] !== undefined 
    ?
    <div className="row">
    <div className="col s12 m6">
      <div className="card red darken-1">
        <div className="card-content white-text">
          <span className="card-title">Errors</span>
          <p>{props[404]}</p>
        </div>
      </div>
    </div>
  </div>
  :
  ''
  )
}

export default Errors