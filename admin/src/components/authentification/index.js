import React, { useState } from "react"
import "../../styles/css/login.css"
import Errors from './Errors'
import { checkAuth, ThunkLogin } from "../../actions/AuthAction"
const LoginForm = (props) => {


const handleClick = (event) => {
    event.preventDefault()
    props.dispatch(ThunkLogin(props.log))

}
const handleChange = (event) => {
    event.preventDefault()
    props.dispatch(checkAuth(event.target.id, event.target.value))
}

   return(
<>
<Errors {...props.resp} />
    <div className="row">
            <div className="input-field col s5">
                <input 
                    value={props.log.pseudo} 
                    id="pseudo" type="text" 
                    className="Validate"  
                    style={{borderColor: 'pink'}} 
                    onChange={handleChange} 
                    />
                <label 
                    className="Pseudo" 
                    htmlFor="pseudo" 
                    style={{color:'pink'}
                    }>
                    Pseudo
                </label>
            </div>
    </div>
     <div className="row">
        <div className="input-field col s5">
            <input 
                value={props.log.pass} 
                id="pass" 
                type="text" 
                className="Validate"  
                style={{borderColor: 'pink'}} 
                onChange={handleChange} 
                />
            <label 
                className="Password" 
                htmlFor="pass" 
                style={{color:'pink'}
                }>
                Mot de passe
            </label>
        </div>
    </div>
    <button 
        className="btn waves-effect waves-light" 
        type="submit" 
        name="action" 
        style={{backgroundColor:'cornflowerblue'}} 
        onClick={handleClick}>
            S'identifier
        <i className="material-icons right">send</i>
    </button>
</>
   )

}
export default LoginForm