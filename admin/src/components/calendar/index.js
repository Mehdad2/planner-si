import React, { useState , useRef} from "react"
import "../../styles/css/calendar.css"

//el.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"})
const Calendar = () => {
    const modal = useRef(null);
    const days = ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi', 'Dimanche'];
    const months = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Decembre'];
    const hours = ['00',1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    var nowDate = new Date();
    const numrows = 7;
    var day = days[ nowDate.getDay() ];
    var dayNumber = nowDate.getDate();
    var hour = hours[nowDate.getHours()]
    var minutesBase = nowDate.getMinutes() + 10
    const hourRef = useRef(hour);
    var month = months[ nowDate.getMonth() ];
    const [date, setDate] = useState({number:"",day:"", month:"", hour:"", dhour:""})
    const [minutes, setMinutes ] = useState(minutesBase)
    const daysWeeks = []
    const dateFullOfWeeks = []
    const handleClick = (element, h, d = '00') => {
        setDate({number:element.number,day: element.dayWeek, month: element.month, hour: h, dhour:d})
        var el = document.getElementById(modal.current.id)

        el.style.display == "block" ? el.style.display = "none" : el.style.display = "block"
      }
      const handleCurrentHour = () => {
          var el = document.getElementById(hourRef.current.id)
          console.log(hourRef)
          return el.scrollIntoView({behavior: "auto", block: "start", inline: "nearest"})
      }

    for (var i = 0; i < numrows; i++) (

        daysWeeks.push( 
            <th key={i + 1} className={day == days[i] ? 'Active': i + 1}>
                {days[i]}
                {" "}
                {day == days[i] ? dayNumber : new Date(new Date().setDate(new Date().getDate() + i)).getDate()}
            </th>
           ) &&
           dateFullOfWeeks.push({ "month": months[new Date(new Date().setDate(new Date().getDate() + i)).getMonth()] , "number" : new Date(new Date().setDate(new Date().getDate() + i)).getDate(),"dayWeek": days[i]  })
   
   
      )
      console.log(dateFullOfWeeks);
     // var modal = document.getElementById("myModal");
return (

    <>
  <div id="myModal" className="modal" ref={modal} >
    <div className="modal-content">
    <span className="close" onClick={handleClick}>&times;</span>
<p>{`${date.day} ${date.number} ${date.month} à ${date.hour}h${date.dhour}`}</p>  
</div>
</div>
    <div className="row">
    <div className="col s12"><h5>{`${month} ${nowDate.getFullYear()}`}</h5></div>
    </div>
    <div id="table-scroll" className="table-scroll">
    <table id="main-table" className="main-table">
        <thead>
        <tr>
        <th className="hourColumn"></th>
        {daysWeeks.map((element) => (
            element
            )
            
        )}
        </tr>

        </thead>

        <tbody>
            {hours.map((h, i) => 
                <tr key={`h${i}`} id="same">
                    <td key={`h${i}`} id={`h ${h}`} className={hour == h ? 'Active': `h${i}`} ref={h == hour ? hourRef : null}>{`${h} h`}</td>
                    {dateFullOfWeeks.map((element) => 
                    <td key={`ceil${element.number}`} onLoadedMetadata={handleCurrentHour}>
                          {
                          element.number == dayNumber && hour == h ? 
                          <>
                          <div style={{marginTop: minutes + 'px', color: 'purple', borderTop: '2px solid'}}></div>
                          <div class="top_row" onDoubleClick={() => handleClick(element, h)} style={{marginTop: '-' + `${minutes + 6 }` + 'px'}}><div></div></div>
                          <div class="top_row" onDoubleClick={() => handleClick(element, h, 30)} ><div></div></div>
                          </>
                          : 
                          <>
                          <div class="top_row" onDoubleClick={() => handleClick(element, h)}><div></div></div>
                          <div class="top_row" onDoubleClick={() => handleClick(element, h, 30)}><div></div></div>
                          </>

                        }
                    </td>

                    )}
                </tr>
            )}
            
    
        </tbody>
    </table>
    </div>
    {window.addEventListener('load', () => {
        handleCurrentHour()
    })}
   {setInterval(() =>  setMinutes(new Date().getMinutes() + 10), nowDate.getMinutes())}
  </>
  

  
        
)

}

export default Calendar;