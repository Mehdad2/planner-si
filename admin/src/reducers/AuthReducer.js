import { auth } from '../actions/types'

const defaultState = {
    log: {
        pseudo: '',
        pass: ''
    },
    response: {},
    isAuthentificate: false

}

const AuthReducer = (state= defaultState, action = {}) => {
    switch(action.type) {
        case auth.CHECK_LOGIN:
            return {
                ...state,
                log: {
                    ...state.log,
                   [action.id] : action.name
                }
            }
        case auth.SET_ERRORS:
            return {
                ...state,
                response: {
                    [action.code] : action.message
                }
            }
        case auth.SET_STATUSAUTH:
            return {
                ...state,
                isAuthentificate: !state.isAuthentificate
            }
        default :
        return state
    }

}
export default AuthReducer