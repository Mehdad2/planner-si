import React from "react";
import ReactDOM from "react-dom";
import { store } from "./app/store"
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from 'react-router-dom'
//const  common  = require('common-plannersi');
import 'css/materialize.min.css'

import App from "./app/App";


ReactDOM.render(
  <Provider store={store}>
         <Router> 
            <App />
        </Router>
    </Provider>,
  document.getElementById("app")
);