const  common  = require('common-plannersi');
const webcommon = require('./webpack.common.js');

 module.exports = common.merge(webcommon, {
   mode: 'production',
 });