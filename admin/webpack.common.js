 const path = require('path');
 const  common  = require('common-plannersi');
 //const { CleanWebpackPlugin } = require('./node_modules/common-plannersi/node_modules/clean-webpack-plugin');
 //const HtmlWebpackPlugin = require('./node_modules/common-plannersi/node_modules/html-webpack-plugin');
const pathCommon = common.pathFromOuside
 module.exports = {
  resolve: { 
    modules: [path.resolve(__dirname), "node_modules",
    path.resolve(__dirname), pathCommon
  ]
  },
   entry: {
     app: './src/index.js',
   },
    // Modules
  module: {
    rules: [
      // JS
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          // babel avec une option de cache
          {
            loader: `${pathCommon}/babel-loader`,
            options: {
              cacheDirectory: true,
            },
          },
        ],
      },
      // CSS / SASS / SCSS
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          // style-loader ou fichier
        {
          loader: `${pathCommon}/style-loader`
        },
          // Chargement du CSS
        {
          loader: `${pathCommon}/css-loader`
        }
         
          
          // SASS
          //'sass-loader',
        ],
      },
      // Inages
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/',
            },
          },
        ],
      },
    ],
  },
   plugins: [
     // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
     new common.CleanWebpackPlugin(),
         // Permet de prendre le index.html de src comme base pour le fichier de dist/
    new common.HtmlWebpackPlugin({
        template: './src/index.html',
        filename: './index.html',
      }),
   ],
   output: {
     filename: '[name].bundle.js',
     path: path.resolve(__dirname, 'dist'),
   }
}
