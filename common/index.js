'use strict'
var { CleanWebpackPlugin } = require('clean-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var merge = require('webpack-merge')
var materialize = ('materialize-css')
var pathFromOuside = './node_modules/common-plannersi/node_modules'


module.exports = {
    HtmlWebpackPlugin,
    CleanWebpackPlugin,
    pathFromOuside,
    merge

 };

//module.exports = CleanWebpackPlugin; 
//module.exports = HtmlWebpackPlugin; 